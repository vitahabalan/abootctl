use block_utils::BlockResult;
use block_utils::get_block_devices;
use gptman::GPTPartitionEntry;
use gptman::GPT;
use std::fs::{File, OpenOptions};
use std::path::{Path, PathBuf};

pub type PartitionPathCombo = Vec<(GPTPartitionEntry, PathBuf)>;

fn get_partitions_for_dev(dev_path: &Path) -> gptman::Result<GPT> {
    println!("Testing path {}", dev_path.display());
    let mut dev_file = File::open(dev_path)?;
    gptman::GPT::find_from(&mut dev_file)
}

pub fn get_slot_partitions_device(dev_path: &Path) -> (PartitionPathCombo, PartitionPathCombo) {
    let mut all_partitions = Vec::new();
    match get_partitions_for_dev(dev_path) {
        Ok(p) => {
            for (_, partition) in p.iter() {
                all_partitions.push(partition.clone());
            }
        }
        Err(_) => (),
    }

    let mut a_parts: PartitionPathCombo = Vec::new();
    let mut b_parts: PartitionPathCombo = Vec::new();
    //println!("{:?}", all_partitions);

    for i in all_partitions {
        // Search for slotted partitions
        let name = i.partition_name.as_str();
        let len = name.len();
        //println!("{}", &len);
        if len > 2 {
            let name_end = &name[len - 2..];
            if name_end.eq("_a") {
                a_parts.push((i.clone(), dev_path.to_path_buf()));
            } else if name_end.eq("_b") {
                b_parts.push((i.clone(), dev_path.to_path_buf()));
            }
        }
    }

    (a_parts, b_parts)
}

pub fn get_slot_partitions(devs: Vec<PathBuf>) -> BlockResult<(PartitionPathCombo, PartitionPathCombo)> {
    // Open relevant GPT stuff
    let blockdevs = if devs.is_empty() {
        get_block_devices()?
    } else {
        devs
    };

    let mut a_parts: PartitionPathCombo = Vec::new();
    let mut b_parts: PartitionPathCombo = Vec::new();

    for i in blockdevs {
        // Iterates over block devices and adds slotted partitions
        let (a_ext, b_ext) = get_slot_partitions_device(&i);
        a_parts.extend(a_ext);
        b_parts.extend(b_ext);
    }

    Ok((a_parts, b_parts))
}

pub fn get_boot_partitions(devs: Vec<PathBuf>) -> BlockResult<(GPTPartitionEntry, GPTPartitionEntry, PathBuf)> {
    let (a_parts, b_parts) = get_slot_partitions(devs)?;
    let mut boot_a: Option<GPTPartitionEntry> = None;
    let mut boot_b: Option<GPTPartitionEntry> = None;
    let mut path: Option<PathBuf> = None;
    for i in a_parts {
        if i.0.partition_name.to_string().eq("boot_a") {
            boot_a = Some(i.0);
            path = Some(i.1);
        }
    }
    for i in b_parts {
        if i.0.partition_name.to_string().eq("boot_b") {
            boot_b = Some(i.0);
        }
    }

    // Returns boot partitions specifically
    Ok((boot_a.unwrap(), boot_b.unwrap(), path.unwrap()))
}

pub fn set_partitions(
    new_partitions: PartitionPathCombo,
) -> Result<(), Box<dyn std::error::Error>> {
    //println!("Path: {}", path.to_str());
    let mut to_update: PartitionPathCombo = Vec::new();
    for (part, pathbuf) in new_partitions {
        // Opens disk for each partition. Ugly, but necessary since slot partitions may be written to diff disks
        let disk = get_partitions_for_dev(pathbuf.as_path())?;
        let mut part_table = Vec::new();

        for (_, partition) in disk.iter() {
            part_table.push(partition.clone());
        }
        // Sanity check
        if !part_table.iter().any(|partition| {
            partition
                .partition_name
                .to_string()
                .eq(&part.partition_name.to_string())
        }) {
            panic!("Partition {:?} not found!", part);
        }
        // Look up partitions in current drive and write
        for old_part in part_table {
            if old_part
                .partition_name
                .to_string()
                .eq(&part.partition_name.to_string())
            {
                println!(
                    "Partition name {} matches, updating partition {}",
                    old_part.partition_name, part.partition_name
                );
                to_update.push((part, pathbuf.clone()));
                break;
            }
        }
    }
    // Creates vec of new tables to write
    let mut tables: Vec<(GPT, PathBuf)> = Vec::new();
    for (part, path) in to_update {
        // New device to be added to vec
        if tables
            .iter()
            .any(|(_, paths)| paths.to_str().unwrap() == path.to_str().unwrap())
        {
            for (cur_table, cur_path) in &mut tables {
                if path.to_str().unwrap() == cur_path.to_str().unwrap() {
                    for (_, cur_part) in cur_table.iter_mut() {
                        if cur_part.starting_lba == part.starting_lba
                            && cur_part.attribute_bits != 0
                        {
                            cur_part.clone_from(&part);
                        }
                    }
                }
            }
        }
        // Update existing element
        else {
            let mut cur_table = get_partitions_for_dev(&path)?;
            //println!("{:?}", cur_table);
            for (_, cur_part) in cur_table.iter_mut() {
                if cur_part.starting_lba == part.starting_lba && cur_part.attribute_bits != 0 {
                    cur_part.clone_from(&part);
                    break;
                }
            }
            tables.push((cur_table, path));
        }
    }
    // Flush to device
    for (table, path) in &mut tables {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&path)
            .unwrap();
        //println!("{:?}", table.header);
        match table.write_into(&mut file) {
            Ok(_) => println!("Block device {} updated", path.to_str().unwrap()),
            Err(e) => panic!("Error updating device {}: {}", path.to_str().unwrap(), e),
        }
    }
    Ok(())
}
