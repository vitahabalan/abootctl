use crate::*;

use gptman::GPTPartitionEntry;
use std::convert::TryInto;
use std::mem::swap;
use std::path::PathBuf;

// Return SlotInfo structs for _a and _b slots
pub fn get_slot_info(devs: Vec<PathBuf>) -> (SlotInfo, SlotInfo) {
    let (boot_a, boot_b, _) = partitions::get_boot_partitions(devs).unwrap();
    let slot_a = SlotInfo::from_boot_flags(boot_a.attribute_bits);
    let slot_b = SlotInfo::from_boot_flags(boot_b.attribute_bits);
    //println!("Slot A: {:?}", slot_a);
    //println!("Slot B: {:?}", slot_b);
    (slot_a, slot_b)
}

// Return active slot (0 or 1)
pub fn get_current_slot(devs: Vec<PathBuf>) -> i32 {
    let (slot_a, slot_b) = get_slot_info(devs);
    if (slot_a.is_active() == 1) && (slot_b.is_active() == 0) {
        0
    } else if (slot_a.is_active() == 0) && (slot_b.is_active() == 1) {
        1
    } else {
        panic!(
            "Corrupted headers; none or both partitions marked active: {:#b}, {:#b}",
            slot_a.get_boot_flags(),
            slot_b.get_boot_flags()
        );
    }
}

// Marks slot as successfully booting
pub fn mark_successful(slot: i32, devs: Vec<PathBuf>) {
    let (mut slot_a, mut slot_b) = get_slot_info(devs.clone());
    let (mut boot_a, mut boot_b, dev_path) = partitions::get_boot_partitions(devs).unwrap();

    match slot {
        0 => {
            slot_a.set_boot_successful(1);
            boot_a.attribute_bits = slot_a.get_boot_flags();
        }
        1 => {
            slot_b.set_boot_successful(1);
            boot_b.attribute_bits = slot_b.get_boot_flags();
        }
        _ => unsafe { std::hint::unreachable_unchecked() }
    }

    let parts = vec![(boot_a, dev_path.clone()), (boot_b, dev_path)];
    match partitions::set_partitions(parts) {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    }
}

// Marks given slot unbootable
pub fn mark_unbootable(slot: i32, devs: Vec<PathBuf>) {
    let (mut slot_a, mut slot_b) = get_slot_info(devs.clone());
    let (mut boot_a, mut boot_b, dev_path) = partitions::get_boot_partitions(devs).unwrap();

    match slot {
        0 => {
            slot_a.set_is_unbootable(1);
            boot_a.attribute_bits = slot_a.get_boot_flags();
        }
        1 => {
            slot_b.set_is_unbootable(1);
            boot_b.attribute_bits = slot_b.get_boot_flags();
        }
        _ => unsafe { std::hint::unreachable_unchecked() }
    }

    let parts = vec![(boot_a, dev_path.clone()), (boot_b, dev_path)];
    match partitions::set_partitions(parts) {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    }
}

// Is given slot bootable?
pub fn is_bootable(slot: i32, devs: Vec<PathBuf>) -> bool {
    let (slot_a, slot_b) = get_slot_info(devs);
    let unbootable;
    match slot {
        0 => {
            unbootable = slot_a.is_unbootable() != 0;
        }
        1 => {
            unbootable = slot_b.is_unbootable() != 0;
        }
        _ => unsafe { std::hint::unreachable_unchecked() }
    }
    println!("Slot {} is unbootable: {}", slot, unbootable);
    unbootable
}

// Was boot successful on given slot?
pub fn is_successful(slot: i32, devs: Vec<PathBuf>) -> bool {
    let (slot_a, slot_b) = get_slot_info(devs);
    let successful;
    match slot {
        0 => {
            if slot_a.boot_successful() == 1 {
                successful = true;
            } else {
                successful = false;
            }
        }
        1 => {
            if slot_b.boot_successful() == 1 {
                successful = true;
            } else {
                successful = false;
            };
        }
        _ => unsafe { std::hint::unreachable_unchecked() }
    }
    println!("Slot {} marked suceessful: {}", slot, successful);
    successful
}

// Android has this, so we do too
pub fn get_suffix(slot: i32) -> String {
    match slot {
        0 => "_a".to_owned(),
        1 => "_b".to_owned(),
        _ => unsafe { std::hint::unreachable_unchecked() }
    }
}

// Sets slot
pub fn set_slot(slot: i32, devs: Vec<PathBuf>, manually_set: bool) {
    if slot == get_current_slot(devs.clone()) {
    }
    // Only operate if slot needs to actually change
    else {
        let (parts_a, parts_b) = partitions::get_slot_partitions(devs).unwrap();
        // Both slots should have same no. of partitions in the same order
        assert_eq!(parts_a.len(), parts_b.len(), "Number of A-slotted partitions ({}) does not match number of B-slotted partitions ({}); exiting", parts_a.len(), parts_b.len());
        let mut new_parts_a: Vec<(GPTPartitionEntry, PathBuf)> = Vec::new();
        let mut new_parts_b: Vec<(GPTPartitionEntry, PathBuf)> = Vec::new();
        for i in 0..(parts_a.len() - 1) {
            let (mut a_part, path_a) = parts_a[i].clone();
            let (mut b_part, path_b) = parts_b[i].clone();

            // Sanity check
            let mut a_name = a_part.partition_name.to_string();
            a_name.truncate(&a_part.partition_name.as_str().len() - 2);
            let mut b_name = b_part.partition_name.to_string();
            b_name.truncate(&b_part.partition_name.as_str().len() - 2);

            assert!(
                &a_name.eq(&b_name),
                "Partitions {} and {} do not match; exiting",
                &a_part.partition_name.to_string(),
                &b_part.partition_name.to_string()
            );

            // Set flags accordingly
            let mut a_flags = SlotInfo::from_boot_flags(a_part.attribute_bits);
            let a_val = (15 * (slot - 1)).abs().try_into().unwrap();
            a_flags.set_is_active(a_val);
            a_flags.set_is_unbootable(0); // This looks weird but seems to be correct?
            a_flags.set_boot_successful(a_val);
            a_part.attribute_bits = a_flags.get_boot_flags();

            let mut b_flags = SlotInfo::from_boot_flags(b_part.attribute_bits);
            let b_val = (15 * slot).try_into().unwrap();
            b_flags.set_is_active(b_val);
            b_flags.set_is_unbootable(0);
            b_flags.set_boot_successful(0);
            b_part.attribute_bits = b_flags.get_boot_flags();

            // Flip type GUIDs
            swap(
                &mut a_part.partition_type_guid,
                &mut b_part.partition_type_guid,
            );

            new_parts_a.push((a_part, path_a));
            new_parts_b.push((b_part, path_b));
        }

        let mut new_parts = Vec::new();
        new_parts.extend(new_parts_a);
        new_parts.extend(new_parts_b);

        // UFS ioctl query
        match manually_set {
            true => eprintln!("WARNING: Ensure you have manually set the boot slot in the UFS LUN before rebooting"),
            false => ufs::set_ufs_slot(slot)
        }
        
        match partitions::set_partitions(new_parts) {
            Ok(_) => todo!(),
            Err(e) => panic!("{}", e),
        }
    }
}